// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function fullName(nameObject){
    if(!nameObject || nameObject.constructor !== Object || Object.keys(nameObject).length === 0){
        return '';
    }
    let nameFull = '';
    //first name
    let first_name = nameObject.first_name.toLowerCase();
    first_name = first_name[0].toUpperCase() + first_name.slice(1);
    nameFull += first_name;
    //middle name
    if(nameObject.middle_name){
        let middle_name = nameObject.middle_name.toLowerCase();
        middle_name = middle_name[0].toUpperCase() + middle_name.slice(1);
        nameFull = nameFull + ' ' + middle_name;
    }
    //last name
    let last_name = nameObject.last_name.toLowerCase();
    last_name = last_name[0].toUpperCase() + last_name.slice(1);
    nameFull = nameFull + ' ' + last_name;

    return nameFull;
}

module.exports = fullName;