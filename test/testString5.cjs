// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

let combineStrings = require('../string5.cjs');

console.log(combineStrings(["the", "quick", "brown", "fox"]));
console.log(combineStrings(['abc']));
console.log(combineStrings());
console.log(combineStrings({obj:'object'}));