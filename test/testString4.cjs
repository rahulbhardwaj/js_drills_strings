// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

let fullName = require('../string4.cjs');

console.log(fullName({"first_name": "JoHN", "last_name": "SMith"}));
console.log(fullName({"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}));
console.log(fullName());
console.log(fullName({}));
console.log(fullName(['array']));