// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 

let numFromString = require('../string1.cjs');

console.log(numFromString('$100.45'));
console.log(numFromString('$1002.22'));
console.log(numFromString('-$123'));
console.log(numFromString());
console.log(numFromString(100));