// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

let getMonth = require('../string3.cjs');

console.log(getMonth('10/1/2021'));
console.log(getMonth());
console.log(getMonth(['array']));
console.log(getMonth('10/20/30/40'));
console.log(getMonth('10/l/2021'))
console.log(getMonth('10/15/2021'));