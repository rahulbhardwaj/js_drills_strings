// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

let componentsOfIP = require('../string2.cjs');

console.log(componentsOfIP('111.193.161.143'));
console.log(componentsOfIP('100.180.l.5'));
console.log(componentsOfIP('1.2.3.4.5.5'));
console.log(componentsOfIP());
console.log(componentsOfIP(['array']));