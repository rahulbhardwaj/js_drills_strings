// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 

function numFromString(srtingWithNum){
    if (!srtingWithNum || srtingWithNum.constructor !== String){
        return 0;
    }
    let answer = 0;
    let isNegative = false;
    let isDelimalStarted = false;
    let delimalPrecion = 1;
    for(let i=0;i<srtingWithNum.length;i++){
        if(srtingWithNum[i] === '-'){
            isNegative = true;
            continue;
        }
        if(srtingWithNum[i] == '.'){
            isDelimalStarted = true;
            continue;
        }
        const n = srtingWithNum.charCodeAt(i);
        if(n>47 && n<58){
            if(isDelimalStarted){
                delimalPrecion /= 10;
                answer = answer + parseInt(srtingWithNum[i])*delimalPrecion;
            }
            else{
                answer = answer*10 + parseInt(srtingWithNum[i]);
            }
        }
    }
    return isNegative ? answer*(-1)  : answer;
}

module.exports = numFromString;