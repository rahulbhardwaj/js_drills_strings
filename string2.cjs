// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].
// Support IPV4 addresses only. If there are other characters detected, return an empty array.

function componentsOfIP(ipAddress){
    if(!ipAddress || ipAddress.constructor != String){
        return [];
    }
    let answer = ipAddress.split('.');
    if(answer.length > 4){
        console.log();
        return [];
    }
    for(let ip in answer){
        answer[ip] = parseInt(answer[ip]);
    }
    for(let ip in answer){
        if(!answer[ip]){
            return [];
        }
    }
    return answer;
}

module.exports = componentsOfIP;