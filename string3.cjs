// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

function getMonth(dateString){
    if(!dateString || dateString.constructor !== String){
        return 0;
    }
    let dateSplit = dateString.split('/');
    if(dateSplit.length > 3){
        return 0;
    }
    for(const index in dateSplit){
        if(!parseInt(dateSplit[index])){
            return 0;
        }
    }
    let month = parseInt(dateSplit[1]);
    return month<=12 ? month : 0;
}

module.exports = getMonth;